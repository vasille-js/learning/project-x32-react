import React, { Component } from "react";
import { BetonBlock, BlockPack, BrickBlock } from "./types/block";
import MapX1 from "./components/x1/MapX1";
import "./index.css";

const s = 40;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blocks: new BlockPack(0, 0, 26 * s, 26 * s)
    };

    this.createLevel([
      [2, 1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [1, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 2, 2, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 2, 2, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],

      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [0, 0, 0, 0, 0, 1, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 1, 1, 1, 0, 0,  0, 0],
      [2, 2, 0, 0, 1, 1, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 1, 1, 1, 0, 0,  2, 2],
      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],

      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 1, 1, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 1, 1, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],
      [0, 0, 1, 1, 0, 0, 1, 1,  0, 0, 0, 1, 1, 1, 1, 0,  0, 0, 1, 1, 0, 0, 1, 1,  0, 0],

      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 1, 0, 0, 1, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 1, 0, 0, 1, 0,  0, 0, 0, 0, 0, 0, 0, 0,  0, 0]
    ]);
  }

  static propTypes = {};
  static defaultProps = {};

  createLevel(data) {
    for (let i in data) {
      for (let j in data[i]) {
        let v = data[i][j];

        if (v !== 0) {
          if (v === 1) {
            this.state.blocks.els.push(new BrickBlock(j * s, i * s));
          } else if (v === 2) {
            this.state.blocks.els.push(new BetonBlock(j * s, i * s));
          }
        }
      }
    }

    let fastFind = {
      0: {},
      1: {},
      2: {},
      3: {}
    };
    let ls = 8 * s;

    for (let x = 0; x < 4; x++) {
      for (let y = 0; y < 4; y++) {
        let blockPack = new BlockPack(x * ls, y * ls, ls, ls);
        this.state.blocks.sub.push(blockPack);
        fastFind[x][y] = blockPack;
      }
    }

    for (let block2 of this.state.blocks.els) {
      let block = block2;
      fastFind[Math.floor(block.x / ls)][Math.floor(block.y / ls)].els.push(
        block
      );
    }

    for (let i = 0; i < this.state.blocks.sub.length;) {
      const sub = this.state.blocks.sub[i];
      if (sub.els.length === 0) {
        this.state.blocks.sub.splice ( i, 1 );
      } else {
        i++;
      }
    }

    ls = 2 * s;

    for (let pack2 of this.state.blocks.sub) {
      let pack = pack2;

      for (let x = 0; x < 4; x++) {
        for (let y = 0; y < 4; y++) {
          let blockPack = new BlockPack(
            pack.x + x * ls,
            pack.y + y * ls,
            ls,
            ls
          );
          pack.sub.push(blockPack);
          fastFind[x][y] = blockPack;
        }
      }

      for (let block2 of pack.els) {
        let block = block2;
        fastFind[Math.floor((block.x - pack.x) / ls)][
          Math.floor((block.y - pack.y) / ls)
        ].els.push(block);
      }

      for (let i = 0; i < pack.sub.length;) {
        const sub = pack.sub[i];
        if (sub.els.length === 0) {
          pack.sub.splice ( i, 1 );
        } else {
          i++;
        }
      }
    }
  }

  render() {
    const { blocks } = this.state;

    return (
      <MapX1
        width={1040}
        height={1040}
        els={blocks.els}
        sub={blocks.sub}
        x={0}
        y={0}
      ></MapX1>
    );
  }
}
