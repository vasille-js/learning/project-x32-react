import React, { Component } from "react";
import PropTypes from "prop-types";
import memorize from "memoize-one";

export default class BaseBrick extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.bigSquares = [];
    this.mediumSquares = [];
    this.smallSquares = [];

    for (let i = 0; i < 10; i++) {
      this.bigSquares.push({
        x: Math.random() * 30,
        y: Math.random() * 30,
        rotate: Math.random() * 90,
        step: i
      });
    }

    for (let i = 0; i < 40; i++) {
      this.mediumSquares.push({
        x: Math.random() * 35,
        y: Math.random() * 35,
        rotate: Math.random() * 90,
        step: i
      });
    }

    for (let i = 0; i < 50; i++) {
      this.smallSquares.push({
        x: Math.random() * 38,
        y: Math.random() * 38,
        rotate: Math.random() * 90,
        step: i
      });
    }
  }

  static propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    tx: PropTypes.number,
    ty: PropTypes.number,
    l1x: PropTypes.number,
    l1y: PropTypes.number,
    l2x: PropTypes.number,
    l2y: PropTypes.number,
    step: PropTypes.number,
    scale: PropTypes.number,
    cName: PropTypes.string,
    cw: PropTypes.number,
    ch: PropTypes.number,
    vl: PropTypes.number
  };
  static defaultProps = {};

  cx = memorize((x, l1x, l2x, step) =>
      step < 2 || step > 4
          ? x * step
          : step === 2
          ? 2 * (x - l1x)
          : 4 * (x - l2x));

  cy = memorize((y, l1y, l2y, step) =>
      step < 2 || step > 4
          ? y * step
          : step === 2
          ? 2 * (y - l1y)
          : 4 * (y - l2y));

  lscale = memorize((scale, step) => scale / step);
  lWidth = memorize(step => 40 * step, this.step);
  lHeight = memorize(step => 40 * step);
  left = memorize((x, scale, tx) => x * scale + tx);
  right = memorize((left, w, scale) => left + w * scale);
  top = memorize((y, scale, ty) => y * scale + ty);
  bottom = memorize((top, h, scale) => top + h * scale);
  hVisible = memorize((left, right, cw) => left < cw && right > 0);
  vVisible = memorize((top, bottom, ch) => top < ch && bottom > 0);
  visible = memorize((h, v) => h && v);

  render() {
    const { cName, step, x, y, tx, ty, l1x, l1y, l2x, l2y, scale, cw, ch, vl } = this.props;
    const lWidth = this.lWidth(step);
    const lHeight = this.lHeight(step);
    const cx = this.cx(x, l1x, l2x, step);
    const cy = this.cy(y, l1y, l2y, step);
    const left = this.left(x, scale, tx);
    const top = this.top(y, scale, ty);
    const lscale = this.lscale(scale, step);
    const visible = this.visible(
        this.hVisible(left, this.right(left, lWidth, lscale), cw),
        this.vVisible(top, this.bottom(top, lHeight, lscale), ch)
    );

    if (visible) return <div
        className={`block ${cName}`}
        style={{
          'transform': !visible ? '' :
              step <= 4
                  ? 'translate(' + cx + 'px, ' + cy + 'px)'
                  : 'translate(' + left + 'px, ' + top + 'px) scale(' + lscale + ')',
          'willChange': step > 4 ? 'transform' : ''
        }}
    >
      {(() => {
        if (vl >= 1) {
          return (<div>
            {this.bigSquares.map(item => {
              if (scale >= 3 + 0.1 * step) {
                return (<div
                    style={ {
                      transform : 'translate(' + item.x * step + 'px, ' + item.y * step + 'px) rotate(' + item.rotate + 'deg)',
                      width : step * 0.35,
                      height : step * 0.35,
                    } }
                />);
              }
              else return (<div />)
            })}
          </div>)
        }
        return <div/>;
      })()}

      {(() => {
        if (vl >= 2) {
          return (<div>
            {this.mediumSquares.map(item => {
              if (scale >= 9 + 0.15 * step) {
                return (<div
                    style={ {
                      transform : 'translate(' + item.x * step + 'px, ' + item.y * step + 'px) rotate(' + item.rotate + 'deg)',
                      width : step * 0.15,
                      height : step * 0.15
                    } }
                />);
              }
              else return <div/>;
            })}
          </div>)
        }
        return <div/>;
      })()}

      {(() => {
        if (vl >= 3) {
          return (<div>
            {this.smallSquares.map(item => {
              if (scale >= 17 + 0.2 * step) {
                return (<div
                    style={ {
                      transform : 'translate(' + item.x * step + 'px, ' + item.y * step + 'px) rotate(' + item.rotate + 'deg)',
                      width : step * 0.05,
                      height : step * 0.05
                    } }
                />);
              }
              else return <div/>;
            })}
          </div>)
        }
        return <div/>;
      })()}
    </div>;
    else return <div />;
  }
}
