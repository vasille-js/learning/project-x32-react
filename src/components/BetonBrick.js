import React, { Component } from "react";
import PropTypes from "prop-types";
import BaseBrick from "./BaseBrick";

export default class BetonBrick extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    tx: PropTypes.number,
    ty: PropTypes.number,
    l1x: PropTypes.number,
    l1y: PropTypes.number,
    l2x: PropTypes.number,
    l2y: PropTypes.number,
    step: PropTypes.number,
    scale: PropTypes.number,
    cw: PropTypes.number,
    ch: PropTypes.number,
    vl: PropTypes.number
  };
  static defaultProps = {};

  render() {
    const { scale, ch, cw, l1x, l1y, l2x, l2y, step, tx, ty, x, y, vl } = this.props;

    return (
      <BaseBrick
        scale={scale}
        ch={ch}
        cName={"beton-brick"}
        cw={cw}
        l1x={l1x}
        l1y={l1y}
        l2x={l2x}
        l2y={l2y}
        step={step}
        tx={tx}
        ty={ty}
        x={x}
        y={y}
        vl={vl}
      />
    );
  }
}
