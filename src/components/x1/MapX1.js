import React, { Component } from "react";
import PropTypes from "prop-types";
import MapX2 from "./MapX2";
import memorize from "memoize-one";

export default class MapX1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tx: 0,
      ty: 0,
      scale: 1,
      cw: 0,
      ch: 0,
      rtx: 0,
      rty: 0,
      rscale: 0,
      btx: 0,
      bty: 0,
      bscale: 0,
      beginTime: 0,
      endTime: 0,
      isAnim: false
    };
  }

  static propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    width: PropTypes.number,
    height: PropTypes.number,
    els: PropTypes.array,
    sub: PropTypes.array
  };
  static defaultProps = {};

  componentDidMount() {
    document.onmousedown = press => {
      let tx = this.state.tx;
      let ty = this.state.ty;
      press.preventDefault();

      document.onmousemove = move => {
        let rtx = tx + (move.screenX - press.screenX);
        let rty = ty + (move.screenY - press.screenY);
        this.setState({
          tx: rtx,
          rtx: rtx,
          ty: rty,
          rty: rty
        });
        move.preventDefault();
      };

      document.onmouseup = up => {
        document.onmousemove = null;
        document.onmouseup = null;
        up.preventDefault();
      };
    };

    document.onwheel = wheel => {
      let step = this.stepScale(this.state.scale);
      let cscale = this.state.rscale || this.state.scale;
      let ctx = this.state.rtx || this.state.tx;
      let cty = this.state.rty || this.state.ty;
      let nscale;

      if (wheel.deltaY < 0) {
        let v = cscale + 0.1 * step;

        if (v < 32) {
          nscale = v;
        } else {
          nscale = 32;
        }
      } else {
        let v = cscale - 0.1 * step;

        if (v > 0.5) {
          nscale = v;
        } else {
          nscale = 0.5;
        }
      }

      this.animTo(
        ctx - ((nscale - cscale) * (wheel.clientX - ctx)) / cscale,
        cty - ((nscale - cscale) * (wheel.clientY - cty)) / cscale,
        nscale
      );
    };

    window.onresize = () => {
      this.setState({
        cw: window.innerWidth,
        ch: window.innerHeight
      });
    };

    window.onkeypress = () => {
      document.onwheel({deltaY: -1, clientX: 0, clientY: 0});
    }

    window.onresize();
  }

  animTo(x, y, scale) {
    this.setState({
      rtx: x
    });
    this.setState({
      rty: y
    });
    this.setState({
      rscale: scale
    });
    this.setState({
      btx: this.state.tx
    });
    this.setState({
      bty: this.state.ty
    });
    this.setState({
      bscale: this.state.scale
    });
    this.setState({
      beginTime: new Date().getTime()
    });
    this.setState({
      endTime: this.state.beginTime + 300
    });

    if (!this.state.isAnim) {
      this.setState({
        isAnim: true
      });

      let update = () => {
        let now = new Date().getTime();

        if (now > this.state.endTime) {
          this.setState({
            isAnim: false
          });
          this.setState({
            tx: this.state.rtx
          });
          this.setState({
            ty: this.state.rty
          });
          this.setState({
            scale: this.state.rscale
          });
        } else {
          let progress = (now - this.state.beginTime) / 300;
          let step = Math.cos(Math.PI * progress) - 1;
          this.setState({
            tx: (-(this.state.rtx - this.state.btx) / 2) * step + this.state.btx,
            ty: (-(this.state.rty - this.state.bty) / 2) * step + this.state.bty,
            scale:
                (-(this.state.rscale - this.state.bscale) / 2) * step +
                this.state.bscale
          });
          window.requestAnimationFrame(update);
        }
      };

      window.requestAnimationFrame(update);
    }
  }

  stepScale = memorize((scale) => {
    if (scale <= 1) {
      return 1;
    }

    if (scale <= 2) {
      return 2;
    }

    if (scale <= 4) {
      return 4;
    }

    if (scale <= 8) {
      return 8;
    }

    if (scale <= 16) {
      return 16;
    }

    if (scale <= 32) {
      return 32;
    }

    return scale;
  });

  vl = memorize(scale => {
    return scale < 3 ? 0 : scale < 9 ? 1 : scale < 17 ? 2 : 3;
  });

  render() {
    const { tx, ty, scale, cw, ch } = this.state;
    const { sub, width, height, x, y } = this.props;
    const stepScale = this.stepScale(scale);
    const vl = this.vl(scale);

    return (
      <div
          className={`map x${stepScale}`}
          style={{
            'width': (stepScale === 1 ? width : 0) + 'px',
            'height': (stepScale === 1 ? height : 0) + 'px',
            'transform': stepScale === 1 ? 'translate(' + (x + tx) + 'px, ' + (y + ty) + 'px) scale(' + scale + ')' : '',
            'position': stepScale === 1 ? 'absolute' : 'static',
            'willChange': stepScale === 1 ? 'transform' : ''
          }}
      >
        {sub.map(item => (
          <MapX2
            x={item.x}
            y={item.y}
            width={item.width}
            height={item.height}
            els={item.els}
            sub={item.sub}
            tx={tx}
            ty={ty}
            scale={scale}
            cw={cw}
            ch={ch}
            stepScale={stepScale}
            vl={vl}
            key={Math.random(item)}
          />
        ))}
      </div>
    );
  }
}
