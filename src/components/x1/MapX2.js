import React, { Component } from "react";
import PropTypes from "prop-types";
import MapX4 from "./MapX4";
import memorize from "memoize-one";

export default class MapX2 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    width: PropTypes.number,
    height: PropTypes.number,
    els: PropTypes.array,
    sub: PropTypes.array,
    tx: PropTypes.number,
    ty: PropTypes.number,
    scale: PropTypes.number,
    cw: PropTypes.number,
    ch: PropTypes.number,
    stepScale: PropTypes.number,
    vl: PropTypes.number
  };
  static defaultProps = {};

  lWidth = memorize((w, step) => w * step);
  lHeight = memorize((h, step) => h * step);
  lScale = memorize((scale, step) => scale / step);
  left = memorize((x, scale, tx) => x * scale + tx);
  right = memorize((left, w, scale) => left + w * scale);
  top = memorize((y, scale, ty) => y * scale + ty);
  bottom = memorize((top, h, scale) => top + h * scale);
  hVisible = memorize((left, right, cw) => left < cw && right > 0);
  vVisible = memorize((top, bottom, ch) => top < ch && bottom > 0);
  visible = memorize((h, v) => h && v);

  render() {
    const { tx, ty, scale, cw, ch, stepScale, x, y, sub, vl } = this.props;
    const lWidth = this.lWidth(this.props.width, stepScale);
    const lHeight = this.lHeight(this.props.height, stepScale);
    const left = this.left(x, scale, tx);
    const top = this.top(y, scale, ty);
    const lScale = this.lScale(scale, stepScale);
    const visible = this.visible(
        this.hVisible(left, this.right(left, lWidth, lScale), cw),
        this.vVisible(top, this.bottom(top, lHeight, lScale), ch));

    if (visible) return (
      <div
          className={'map l1'}
          style={{
            'width': (stepScale === 2 ? lWidth : 0) + 'px',
            'height': (stepScale === 2 ? lHeight : 0) + 'px',
            'transform': stepScale === 2 && visible ? 'translate(' + left + 'px, ' + top + 'px) scale(' + lScale + ')' : '',
            'position': stepScale === 2 ? 'absolute' : 'static',
            'willChange': stepScale === 2 && visible ? 'transform' : ''
          }}
      >
        {sub.map(item => (
          <MapX4
            x={item.x}
            y={item.y}
            width={item.width}
            height={item.height}
            els={item.els}
            sub={item.sub}
            tx={tx}
            ty={ty}
            scale={scale}
            cw={cw}
            ch={ch}
            stepScale={stepScale}
            l1x={x}
            l1y={y}
            vl={vl}
            key={Math.random(item)}
          />
        ))}
      </div>
    );
    else return (<div />);
  }
}
