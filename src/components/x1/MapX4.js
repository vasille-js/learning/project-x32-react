import React, { Component } from "react";
import PropTypes from "prop-types";
import BlockBrick from "../BlockBrick";
import BetonBrick from "../BetonBrick";
import memorize from "memoize-one";

export default class MapX4 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    width: PropTypes.number,
    height: PropTypes.number,
    els: PropTypes.array,
    sub: PropTypes.array,
    tx: PropTypes.number,
    ty: PropTypes.number,
    scale: PropTypes.number,
    cw: PropTypes.number,
    ch: PropTypes.number,
    stepScale: PropTypes.number,
    l1x: PropTypes.number,
    l1y: PropTypes.number,
    vl: PropTypes.number
  };
  static defaultProps = {};

  lWidth = memorize((w, step) => w * step);
  lHeight = memorize((h, step) => h * step);
  lScale = memorize((scale, step) => scale / step);
  left = memorize((x, scale, tx) => x * scale + tx);
  right = memorize((left, w, scale) => left + w * scale);
  top = memorize((y, scale, ty) => y * scale + ty);
  bottom = memorize((top, h, scale) => top + h * scale);
  hVisible = memorize((left, right, cw) => left < cw && right > 0);
  vVisible = memorize((top, bottom, ch) => top < ch && bottom > 0);
  visible = memorize((h, v) => h && v);

  render() {
    const { tx, ty, l1x, l1y, x, y, stepScale, scale, cw, ch, els, vl } = this.props;
    const lWidth = this.lWidth(this.props.width, stepScale);
    const lHeight = this.lHeight(this.props.height, stepScale);
    const left = this.left(x, scale, tx);
    const top = this.top(y, scale, ty);
    const lScale = this.lScale(scale, stepScale);
    const visible = this.visible(
        this.hVisible(left, this.right(left, lWidth, lScale), cw),
        this.vVisible(top, this.bottom(top, lHeight, lScale), ch));

    if (visible) return (
      <div
          className={'map l2'}
          style={{
            'width': (stepScale === 4 ? lWidth : 0) + 'px',
            'height': (stepScale === 4 ? lHeight : 0) + 'px',
            'transform': stepScale === 4 && visible ? 'translate(' + left + 'px, ' + top + 'px) scale(' + lScale + ')' : '',
            'position': stepScale === 4 ? 'absolute' : 'static',
            'willChange': stepScale === 4 && visible ? 'transform' : ''
          }}
      >
        {els.map(item => {
          if (item.type === 1) {
            return (<BlockBrick
                x={item.x}
                y={item.y}
                tx={tx}
                ty={ty}
                l1x={l1x}
                l1y={l1y}
                l2x={x}
                l2y={y}
                step={stepScale}
                scale={scale}
                cw={cw}
                ch={ch}
                vl={vl}
                key={Math.random()}
            />)
          }
          return (<BetonBrick
              x={item.x}
              y={item.y}
              tx={tx}
              ty={ty}
              l1x={l1x}
              l1y={l1y}
              l2x={x}
              l2y={y}
              step={stepScale}
              scale={scale}
              cw={cw}
              ch={ch}
              vl={vl}
              key={Math.random()}
          />)
        })
        }
      </div>
    );
    return <div/>;
  }
}
